<?php
require_once 'classes/User.php';
//Create new users
//Create user 1  and set attributes
$user1 = new User();
$user1->name = "Marc";
$user1->password = "Welkom01";

//Create user 2 and set attributes
$user2 = new User();
$user2->name = "Ana";
$user2->password = "Welkom02";

//Global variable to check if the user is logged
$isLogged = false;
$errorLoging = false;
$msgLogin = "";
$msgError = "";

//Create array with user1 and user2
$userArray = array($user1, $user2);

//Check if it is POST request
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //Check if the inputs have value
    if (($_POST["nameInput"] != null) && ($_POST["passInput"] != null)) {
        //Username input value
        $uname = $_POST['nameInput'];
        //Password input value
        $pass = $_POST['passInput'];
        //Loop the array
        foreach ($userArray as $userLogin) {   //Check if the name and the password are correct
            if ($userLogin->name == $uname && $userLogin->password == $pass) {   //Username and password are correct
                $isLogged = true;
                $msgLogin = "<h3>Welcome, {$userLogin->name}!</h3>";
                //If the are correct, break the loop
                break;
            } else {
                //Display an error message because username or password are wrong
                $errorLoging = true;
                $msgError = "<p>Wrong username or password</p>";
            }
        }
    } else {
        //Display an error message if the user and password are empty
        $errorLoging = true;
        $msgError = "<p>User name and password required</p>";
    }
}
?>
